//============================================================================
// Name        : algoritmoEvolutivo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <math.h>

using namespace std;

struct task {
	string idWork;
	float tam; // cambiar por tam
};

struct edge {
	string id;
	float time;
	vector<int> dependencies;
	vector<int> succes;
	vector<string> input;
	vector<string> output;
};

struct virtualMachine {
	vector<float> intances;
};

struct vMpopulation {
	int vm;
	int cu;
	float tiempoFinal;
};

struct instances {
	float compute;
	float cost;
};

struct gen {
	vector<vMpopulation> individuo;
	float cost;
	float time;
	float reliability;

};

vector<edge> edgeDependencies; // dependencias de cada tarea
vector<task> tamFiles; //tamaño de cada fichero de entrada salida.
vector<gen> poblacion, frenteOptimo, solucionFinal; // tamaño según la cantidad de tarea

int global = 0, numeroPoblacion = 5, numberVM = 3;
float beta = 1, invLanda = 0.0001, alfa = 0.5, relcMax = 0.0, relcMin = 0.0, p =
		0.0, relc = 0.0;
instances vM[3][5];

/*
 * Separa una cadea que pasa por parametros, str seria la cadea y pattern seria
 */
vector<string> split(string str, char pattern) {

	int posInit = 0;
	int posFound = 0;
	string splitted;
	vector<string> resultados;

	while (posFound >= 0) {
		posFound = str.find(pattern, posInit);
		splitted = str.substr(posInit, posFound - posInit);
		posInit = posFound + 1;
		resultados.push_back(splitted);
	}

	return resultados;
}

int containEdge(string edge) {
	int cont = -1;

	for (uint i = 0; i < edgeDependencies.size(); i++) {
		if (edgeDependencies[i].id == edge) {
			return i;
		}
	}
	return cont;
}

float containSize(string id) {
	for (uint i = 0; i < tamFiles.size(); i++) {
		if (tamFiles[i].idWork == id) {
			return tamFiles[i].tam;
		}
	}
	return -1;
}
/*
 * Identifica cada comienzo de línea para clasificar la información
 */
void separateTask(string taskSentence) {

	vector<string> resultados = split(taskSentence, ' ');
	task taskGroup;
	edge edgeGroup;
	int pos = 0, posVec = 0;
	if (resultados[0] == "TASK") { // cambiado a edge
		edgeGroup.id = resultados[1];
		edgeGroup.time = atof(resultados[3].c_str());
		edgeGroup.dependencies.clear();
		edgeGroup.input.clear();
		edgeGroup.output.clear();
		edgeGroup.succes.clear();
		edgeDependencies.push_back(edgeGroup);
	}
	if (resultados[0] == "EDGE") { //cambiado a edge
		//dependencias
		pos = containEdge(resultados[2]);
		if (pos == -1) {
			cout << "Error predecesoras" << endl;
		}
		edgeGroup = edgeDependencies[pos];
		posVec = containEdge(resultados[1]);
		edgeGroup.dependencies.push_back(posVec);
		edgeDependencies[pos] = edgeGroup;

		//sucesivas
		pos = containEdge(resultados[1]);
		if (pos == -1) {
			cout << "Error sucesoras" << endl;
		}
		edgeGroup = edgeDependencies[pos];
		posVec = containEdge(resultados[2]);
		edgeGroup.succes.push_back(posVec);
		edgeDependencies[pos] = edgeGroup;

	}
	if (resultados[0] == "FILE") { //no hace falta cambiar
		taskGroup.idWork = resultados[1];
		taskGroup.tam = atof(resultados[2].c_str());
		tamFiles.push_back(taskGroup);
	}
	if (resultados[0] == "INPUTS") { // cambiado
		pos = containEdge(resultados[1]);
		if (pos == -1)
			cout << "Error en inputs" << endl;
		edgeGroup = edgeDependencies[pos];
		for (uint i = 2; i < resultados.size(); i++) {
			edgeGroup.input.push_back(resultados[i]);
		}
		edgeDependencies[pos] = edgeGroup;
	}
	if (resultados[0] == "OUTPUTS") { // cambiado
		pos = containEdge(resultados[1]);
		if (pos == -1)
			cout << "Error en inputs" << endl;
		edgeGroup = edgeDependencies[pos];
		for (uint i = 2; i < resultados.size(); i++) {
			edgeGroup.output.push_back(resultados[i]);
		}
		edgeDependencies[pos] = edgeGroup;
	}

}

/*
 * Carga el archivo que necesitamos y lo lee, introduciendo los diferentes datos en
 * sus estructuras correspondientes.
 */
void loadFile(string nameFile) {

	ifstream file;
	string setence;

	file.open(nameFile.c_str(), ios::in);

	if (file.is_open()) {
		while (!file.eof()) {

			getline(file, setence);
			separateTask(setence);
		}
	} else {
		cout << "No existe fichero" << endl;
	}
	file.close();

}

//Entrada salida a partir de aquí, para arriba

float startTime(int ti);
float transTime(int ti);
float transTime(int tj, int ti);

int convert(string ti) {
	string conv = "";

	for (uint i = 0; i < ti.size(); i++) {
		if (ti[i] == '0') {
			conv = ti.substr(i, ti.size());

			return atoi(conv.c_str());
		}
	}

	return -1;
}

void initializeVirtualMachines() {
	for (int i = 0; i < 3; i++) {
		if (i == 0) {
			vM[i][0].compute = 1.7;
			vM[i][0].cost = 0.06;

			vM[i][1].compute = 3.75;
			vM[i][1].cost = 0.12;

			vM[i][2].compute = 7.5;
			vM[i][2].cost = 0.24;

			vM[i][3].compute = 15;
			vM[i][3].cost = 0.45;

			vM[i][4].compute = 30;
			vM[i][4].cost = 0.9;

		}
		if (i == 2) {
			vM[i][0].compute = 1;
			vM[i][0].cost = 0.07;

			vM[i][1].compute = 2;
			vM[i][1].cost = 0.14;

			vM[i][2].compute = 4;
			vM[i][2].cost = 0.28;

			vM[i][3].compute = 8;
			vM[i][3].cost = 0.56;

			vM[i][4].compute = 16;
			vM[i][4].cost = 1.12;

		}
		if (i == 1) {
			vM[i][0].compute = 1;
			vM[i][0].cost = 0.05;

			vM[i][1].compute = 2;
			vM[i][1].cost = 0.099;

			vM[i][2].compute = 4;
			vM[i][2].cost = 0.2;

			vM[i][3].compute = 8;
			vM[i][3].cost = 0.401;

			vM[i][4].compute = 16;
			vM[i][4].cost = 0.802;
		}
	}

}

/*
 * Calcular el tiempo de ejecucion
 */
float execTime(int ti) { //done -- bien

	float exec = 0;
	//string tarea = inOutFiles[containFile(ti)].input;
	// saber la m y la k
	exec =
			edgeDependencies[ti].time
					/ vM[poblacion[global].individuo[ti].vm][poblacion[global].individuo[ti].cu].compute;

	return exec; // si devuelve -1, error
}

float waitTime(int tj, int ti) { //done

	vector<int> B;
	int pos = tj;
	float wait = 0;

	B.clear();
	for (uint j = 0;
			j < edgeDependencies[pos].succes.size()
					&& edgeDependencies[pos].succes[j] != ti; j++) {
		B.push_back(edgeDependencies[pos].succes[j]);
	}

	for (uint j = 0; j < B.size(); j++) {
		wait += transTime(tj, B[j]);
	}

	return wait;
}

float receTime(int ti) { //done -- bien

	return transTime(ti) - startTime(ti);

}

float endTime(int ti) { //done --
	float suma = 0.0;
	if (poblacion[global].individuo[ti].tiempoFinal == -1) {
		suma = startTime(ti) + receTime(ti) + execTime(ti);
		poblacion[global].individuo[ti].tiempoFinal = suma;
		return suma;
	} else
		return poblacion[global].individuo[ti].tiempoFinal;

}

float startTime(int ti) { //done --
	int edge = ti;
	float suma = 0, maxTime = 0;
	for (uint i = 0; i < edgeDependencies[edge].dependencies.size(); i++) {
		suma = endTime(edgeDependencies[edge].dependencies[i])
				+ waitTime(edgeDependencies[edge].dependencies[i], ti);
		if (suma > maxTime) {
			maxTime = suma;
		}
	}

	return maxTime;
}

float transTime(int tj, int ti) { //done -- //tengo que comprobar todas las entradas si tuviera o
// solo una o depende de la cantidad de entradas y salidas que tenga la tarea.

//En la misma Iaas 0.1GB/s
//En distinta 0.05Gb/s
	int posout = 0, posin = 0;
	float size = 0.0;
	string in = "", out = "";
	vMpopulation VMti, VMtj;
	posout = tj;
	posin = ti;
	for (uint i = 0; i < edgeDependencies[posout].output.size(); i++) {
		out = edgeDependencies[posout].output[i];
		for (uint j = 0; j < edgeDependencies[posin].input.size(); j++) {
			in = edgeDependencies[posin].input[j];
			if (out == in) {
				size += containSize(in); // tamanio del fichero que le entra a la tarea ti
			}
		}
	}

	VMti = poblacion[global].individuo[ti];
	VMtj = poblacion[global].individuo[tj];

	if (VMti.vm == VMtj.vm) {
		size = size / 100000000;
	} else {
		size = size / 50000000;
	}
	return size;
}

float transTime(int ti) { //done -- comprobar funcion de la documentacion, que no es la misma.

	float suma = 0, sumaMax = 0;
	int tarea = 0;
	for (uint i = 0; i < edgeDependencies[ti].dependencies.size(); i++) {
		tarea = edgeDependencies[ti].dependencies[i];
		suma = endTime(tarea) + waitTime(tarea, ti) + transTime(tarea, ti);
		if (suma > sumaMax) {
			sumaMax = suma;
		}
	}
	return sumaMax;

}

//Tiempo hacia arriba -----------------

float sendTime(int ti) {

	float suma = 0.0;

	for (uint i = 0; i < edgeDependencies[ti].succes.size(); i++) {
		suma += transTime(ti, edgeDependencies[ti].succes[i]);
	}

	return suma;
}

float rentTime(int ti) {
	return receTime(ti) + execTime(ti) + sendTime(ti);
}

float cost(int ti) {
	float minuteTime = 60.0;
	float tenTime = 10.0;
	if (poblacion[global].individuo[ti].vm == 0) { //maquina 1
		float rent = rentTime(ti);
		return (ceil(rent / minuteTime))
				* vM[poblacion[global].individuo[ti].vm][poblacion[global].individuo[ti].cu].cost;
	}
	if (poblacion[global].individuo[ti].vm == 1) { // maquina 2
		float rent = rentTime(ti);
		return (rent
				* vM[poblacion[global].individuo[ti].vm][poblacion[global].individuo[ti].cu].cost)
				/ minuteTime;
	}
	if (poblacion[global].individuo[ti].vm == 2) { // maquina 3
		if (rentTime(ti) <= tenTime) {
			return (tenTime
					* vM[poblacion[global].individuo[ti].vm][poblacion[global].individuo[ti].cu].cost)
					/ minuteTime;
		} else {
			return (rentTime(ti)
					* vM[poblacion[global].individuo[ti].vm][poblacion[global].individuo[ti].cu].cost)
					/ minuteTime;
		}
	}

	return -1;
}

float reliability() {
	float landa = 0.0;
	float suma = 1.0;
	string ti = "";
	for (uint i = 0; i < edgeDependencies.size(); i++) {
		if (poblacion[global].individuo[i].vm == 0)
			landa = 0.001;
		if (poblacion[global].individuo[i].vm == 1)
			landa = 0.002;
		if (poblacion[global].individuo[i].vm == 2)
			landa = 0.003;
		//cout << poblacion[global][y].vm << " " << poblacion[global][y].cu << " "
		//	<< rentTime(ti) << endl;
		suma = suma * exp(-landa * rentTime(i));

	}
	return suma;
}

float costeTotal() {
	float coste = 0.0;
	for (uint i = 0; i < edgeDependencies.size(); i++) {
		coste += cost(i);
	}
	return coste;
}

float tiempoTotal() {
	float maximo = 0, final = 0;
	for (uint i = 0; i < edgeDependencies.size(); i++) {
		if (edgeDependencies[i].succes.size() == 0) {
			final = endTime(i);
			if (final > maximo)
				maximo = final;
		}
	}
	return maximo;
}

float dominancia(gen individuoI, gen individuoJ) {
	float total = false;

	if (individuoI.time >= individuoJ.time
			&& individuoI.cost >= individuoJ.cost) {
		if (individuoI.time > individuoJ.time
				|| individuoI.cost > individuoJ.cost) {
			total = true;
		}
	}

	return total;
}

float distanciaEuclidea(gen individuoI, gen individuoJ, bool vm) {
	float distancia = 0;
	//if (vm == true) {
	for (uint i = 0; i < individuoI.individuo.size(); i++) {
		distancia += pow(
				individuoI.individuo[i].vm - individuoJ.individuo[i].vm, 2);
		distancia += pow(
				individuoI.individuo[i].cu - individuoJ.individuo[i].cu, 2);
	}
	/*
	 } else {
	 for (uint i = 0; i < individuoI.individuo.size(); i++) {
	 distancia += pow(
	 individuoI.individuo[i].cu - individuoJ.individuo[i].cu, 2);

	 }
	 }*/
	return distancia;
}

int rendondear(int numero, bool vm) {
	int resultado = numero;

	if (vm) {
		if (numero > 3)
			resultado = 3;
		else if (numero < 0) {
			resultado = 0;
		}
	} else {
		if (numero > 5)
			resultado = 5;
		else if (numero < 0)
			resultado = 0;
	}

	return resultado;
}

gen swapFireflys3(gen individuoI, gen individuoJ) { // suponemos Bo = 1 y la landa al reves = 0.1, deberia variar entre 0.001 y 100
	int vm = 0, cu = 0;
	gen nuevoInd;
	vMpopulation nuevoVM;
	nuevoInd.individuo.clear();
	//parte vm
	float constanteVM = 0.0, constanteCU = 0.0;
	constanteVM = beta
			* exp(-invLanda * distanciaEuclidea(individuoI, individuoJ, true));
	constanteCU = constanteVM;

	for (uint i = 0; i < individuoI.individuo.size(); i++) {
		vm = individuoI.individuo[i].vm     // parte vm
				+ constanteVM
						* (individuoJ.individuo[i].vm
								- individuoI.individuo[i].vm)
				+ alfa * ((rand() % 100) / 100.0 - 0.5);
		vm = rendondear(vm, true);
		nuevoVM.vm = vm;
		cu = individuoI.individuo[i].cu    // parte cu
				+ constanteCU
						* (individuoJ.individuo[i].cu
								- individuoI.individuo[i].cu)
				+ alfa * ((rand() % 100) / 100.0 - 0.5);

		cu = rendondear(cu, false);
		nuevoVM.cu = cu;
		nuevoVM.tiempoFinal = -1;
		nuevoInd.individuo.push_back(nuevoVM);
	}

	return nuevoInd;
}

gen swapFireflys2(gen individuoI, gen individuoJ) {
	int aprendFrenq = 50;
	for (uint i = 0; i < individuoI.individuo.size(); i++) {
		if (rand() % 101 <= aprendFrenq)
			individuoI.individuo[i] = individuoJ.individuo[i];
	}

	return individuoI;

}

gen swapFireflys(gen individuoI, gen individuoJ) { // suponemos Bo = 1 y la landa al reves = 0.1, deberia variar entre 0.001 y 100
	int vm = 0, cu = 0;
	gen nuevoInd;
	vMpopulation nuevoVM;
	nuevoInd.individuo.clear();
//parte vm
	float constanteVM = 0.0, constanteCU = 0.0;
	constanteVM = beta
			* exp(-invLanda * distanciaEuclidea(individuoI, individuoJ, true));
	constanteCU = constanteVM;
	int ran = 0, ran1 = 0;
	for (uint i = 0; i < individuoI.individuo.size(); i++) {
		ran = (rand() % 100);
		vm = nearbyint(
				individuoI.individuo[i].vm     // parte vm
						+ constanteVM
								* (individuoJ.individuo[i].vm
										- individuoI.individuo[i].vm)
						+ alfa * (((float) ran) / 100.0 - 0.5));
		vm = rendondear(vm, true);
		ran1 = (rand() % 100);
		nuevoVM.vm = vm;
		cu = nearbyint(
				individuoI.individuo[i].cu    // parte cu
						+ constanteCU
								* (individuoJ.individuo[i].cu
										- individuoI.individuo[i].cu)
						+ alfa * (((float) ran1) / 100.0 - 0.5));
		cu = rendondear(cu, false);
		nuevoVM.cu = cu;
		nuevoInd.individuo.push_back(nuevoVM);
	}

	return nuevoInd;
}

void crearPoblacion() {
	vMpopulation machine;
	gen gen;
	gen.individuo.clear();
	int x = 0;
	for (int i = 0; i < numeroPoblacion; i++) {
		for (uint j = 0; j < edgeDependencies.size(); j++) {
			machine.vm = rand() % numberVM; //numero de maquinas
			machine.cu = rand() % 5; //numero de unidad de proceso dentro de la maquina
			machine.tiempoFinal = -1;
			gen.individuo.push_back(machine);
		}
		global = i; // ponemos al invidividuo i
		gen.cost = 0;
		gen.time = 0;
		poblacion.push_back(gen);

		poblacion[i].reliability = reliability();
		while (poblacion[i].reliability < relc) {
			int numero = rand() % 10;
			for (int t = 0; t < numero; t++) {
				x = rand() % poblacion[i].individuo.size();
				if (rand() % 2 == 1) {
					if (poblacion[i].individuo[x].vm != 0) {
						poblacion[i].individuo[x].vm--;
					}
				} else {
					if (poblacion[i].individuo[x].cu != 4) {
						poblacion[i].individuo[x].cu++;
					}
				}
			}
			poblacion[i].reliability = reliability();
		}

		poblacion[i].cost = costeTotal();
		poblacion[i].time = tiempoTotal();
		gen.individuo.clear();
	}
}

void fastNonDominatedSort() {
	for (uint i = 0; i < poblacion.size(); i++) {
		int np = 0;
		for (uint j = 0; j < poblacion.size(); j++) {
			if (dominancia(poblacion[i], poblacion[j])) {
				np++;
			}
		}
		if (np == 0) {
			frenteOptimo.push_back(poblacion[i]);
		}
	}
}

void calcularRel() {
	float landa = 0.001;
	float suma = 1.0;
	string ti = "";
//mejor caso
	for (uint i = 0; i < edgeDependencies.size(); i++) {
		suma = suma * exp(-landa * (edgeDependencies[i].time / 30));
	}
	relcMax = suma;

	suma = 1.0;
	landa = 0.003;
	for (uint i = 0; i < edgeDependencies.size(); i++) {
		suma = suma * exp(-landa * edgeDependencies[i].time);
	}
	relcMin = suma;
}

//fichero, tamaño poblacion ,numero evaluaciones, alfa, beta, gamma, p -lm para compilar
int main(int argc, char *argv[]) {
// 50000 evaluaciones
	gen nuevoInd, viejoInd;
	int anterior = 0, evaluaciones = 5, evaluacionPorVuelta = 0;
	if (argc != 9) {
		cout
				<< "fichero, tamaño poblacion ,numero evaluaciones, alfa, beta, gamma, p -lm para compilar"
				<< endl;
		return 0;
	}
	srandom(time(NULL) + atoi(argv[8]));
	numeroPoblacion = atoi(argv[2]);
	evaluaciones = atoi(argv[3]);
	alfa = atof(argv[4]);
	beta = atof(argv[5]);
	invLanda = atof(argv[6]);
	p = atof(argv[7]);

	//cout << "Carga de datos..." << endl;
	loadFile(argv[1]);

	//cout << "Calculando Relc..." << endl;
	calcularRel();
	relc = relcMin + p * (relcMax - relcMin);

	//cout << "Inicializando valores VM..." << endl;
	initializeVirtualMachines();

	//cout << "Creando población..." << endl;
	crearPoblacion();

	global = 0;
	int x = 0;
	//cout << "Procesando poblacion..." << endl;
	int cont = 0;
	while (anterior < evaluaciones) {
		evaluacionPorVuelta = anterior;
		for (uint i = 0; i < poblacion.size() && anterior < evaluaciones; i++) {
			for (uint j = 0; j < poblacion.size() && anterior < evaluaciones;
					j++) {
				if (dominancia(poblacion[i], poblacion[j])) { //  si entra, es dominado
					global = i;
					anterior++; //sumamos una a evaluaciones conseguidas
					//cout << anterior << endl;
					nuevoInd = swapFireflys3(poblacion[i], poblacion[j]);
					viejoInd = poblacion[i];
					poblacion[i] = nuevoInd;
					poblacion[i].reliability = reliability();
					//cout << i << " " << poblacion[i].reliability << endl;
					while (poblacion[i].reliability < relc) {

						int numero = rand() % 10;
						for (int t = 0; t < numero; t++) {
							x = rand() % poblacion[i].individuo.size();
							if (rand() % 2 == 1) {
								if (poblacion[i].individuo[x].vm != 0) {
									poblacion[i].individuo[x].vm--;
								}
							} else {
								if (poblacion[i].individuo[x].cu != 4) {
									poblacion[i].individuo[x].cu++;
								}
							}
						}
						poblacion[i].reliability = reliability();

					}
					nuevoInd.time = tiempoTotal();
					nuevoInd.cost = costeTotal();
					poblacion[i] = nuevoInd;
					if (dominancia(viejoInd, nuevoInd) == false) {
						poblacion[i] = viejoInd;
					} else
						cont++;
				}
			}
		}
		if (anterior == evaluacionPorVuelta) {
			//cout << "Estancamiento..." << endl;
			for (uint len = 0; len < poblacion.size(); len++) {
				solucionFinal.push_back(poblacion[len]);
			}
			poblacion.clear();
			//cout << "Creando poblacion nueva..." << endl;
			crearPoblacion();
			anterior += 50;
		}
	}
//introducimos la poblacion a la solucion final, por si se ha producido estancamiento
	for (uint i = 0; i < solucionFinal.size(); i++) {
		poblacion.push_back(solucionFinal[i]);
	}

	//cout << "Calculando Fast Non Dominated Sort..." << endl;
	fastNonDominatedSort();

	for (uint i = 0; i < frenteOptimo.size(); i++) {
		cout << frenteOptimo[i].time << " " << frenteOptimo[i].cost << endl;
	}

	//cout << "Fin" << endl;
	return 0;
	//control de parametros, 8
	//introducir semilla random mas parametro
}
